let x = $(document);
x.ready(inicializarEventos);

function inicializarEventos() {
  let x = $("#boton1");
  x.click(presionSubmit);
}

function presionSubmit() {
  let v = $("#dni").val();
  $.getJSON("pagina1.php", { dni: v }, llegadaDatos);
  return false;
}

function llegadaDatos(datos) {
  $("#resultados").html("Nombre:" + datos.nombre +
    "<br>" + "Apellido:" +
    datos.apellido + "<br>" +
    "Direccion:" + datos.direccion);
}